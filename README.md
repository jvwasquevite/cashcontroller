# CashController
Hut8 Trainee Sprint #1

<h2>Funcionabilidades adicionais:</h2>
1. Responsividade mobile por media queries;* <br>
2. Gráfico interativo usando o Google Charts. <br><br>
<i>*Não foi criado a versão para tablets;</i> <br>

<h2>Alguns scripts/frameworks open source que foram utilizados durante o projeto:</h2>
1. Google Fonts (fonts.google.com)<br>
    <i>Roboto Font and Roboto Slab Font;</i><br><br>
2. FontAwesome (fontawesome.com)<br>
    <i>Inserção de ícones na tela.</i><br><br>
3. Google Charts (developers.google.com/chart)<br>
   <i> Ferramenta Google para criação de gráficos em javascript;</i><br>